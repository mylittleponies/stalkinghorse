﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PonyCtrl : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] AudioClip[] neighs;
    [SerializeField] AudioClip[] soundRandom;

    [SerializeField] AudioSource asAlarm;
    [SerializeField] AudioSource asRandom;

    Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Vector3.Distance(startPosition, transform.position) > 0.3f)
        {
            startPosition = transform.position;

            animator.SetTrigger("Alarm");

            if (!asAlarm.isPlaying)
            {
                asAlarm.clip = neighs[(int)Random.Range(0, neighs.Length)];
                asAlarm.Play();
            }
        }

        if (Random.Range(0,9600) == 0 && !asRandom.isPlaying && !asAlarm.isPlaying)
        {
            asRandom.clip = soundRandom[(int)Random.Range(0, soundRandom.Length)];
            asRandom.Play();
        }
    }
}
