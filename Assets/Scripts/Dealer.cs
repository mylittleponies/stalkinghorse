﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dealer : MonoBehaviour
{
    [SerializeField] float lookingDuration;
    [SerializeField] float notLookingDuration;
    [SerializeField] float attentionDuration;
    [SerializeField] AnimationCurve growingSize;

    [SerializeField] Sprite headSleeping;
    [SerializeField] Sprite headAwake0;
    [SerializeField] Sprite headAwake1;
    [SerializeField] Sprite headAttention;

    [SerializeField] AudioClip[] snoringClips;

    float lookingTimer = 0;
    float attentionTimer = 0;

    bool dealerLooking = false;
    bool seeingHorse = false;

    CanvasCtrl canvasCtrl;
    SpriteRenderer headSR;
    ParticleSystem sleepingParticles;
    AudioSource auSo;

    // Start is called before the first frame update
    void Start()
    {
        canvasCtrl = GameObject.Find("Canvas").GetComponent<CanvasCtrl>();

        headSR = transform.Find("HeadSprite").GetComponent<SpriteRenderer>();
        headSR.sprite = headSleeping;

        sleepingParticles = transform.Find("Sleeping").GetComponent<ParticleSystem>();

        auSo = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (dealerLooking)
        {
            //looking timer
            if (lookingTimer < lookingDuration) lookingTimer += Time.deltaTime;
            else
            {
                lookingTimer = 0;
                dealerLooking = false;
                headSR.sprite = headSleeping;
                sleepingParticles.Play();
            }
        }
        else
        {
            //looking timer
            if (lookingTimer < notLookingDuration) lookingTimer += Time.deltaTime;
            else
            {
                lookingTimer = 0;
                dealerLooking = true;
                headSR.sprite = headAwake0;
                sleepingParticles.Stop();
            }
        }

        if (dealerLooking)
        {
            if (GLOBAL.attention > 0) headSR.sprite = headAttention;
            else {
                int whichAwaken = (int)Time.time % 2;

                if (whichAwaken == 0) headSR.sprite = headAwake0;
                else headSR.sprite = headAwake1;
            }
        }
        else
        {
            if (!auSo.isPlaying)
            {
                auSo.clip = snoringClips[(int) Random.Range(0, snoringClips.Length)];
            }
        }
    }

    private void LateUpdate()
    {

    }

    public void SeeingTheHorse()
    {
        if (dealerLooking)
        {
            if (!seeingHorse)
            {
                seeingHorse = true;
            }
            canvasCtrl.AddAttention(transform, 1f);
        }
    }

    void OnColliderEnter(Collider other)
    {
        Debug.Log(other.name);

        if (other.tag == "Player") Debug.Log("seeing");
    }
}
