﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;

public class HorseControl : MonoBehaviour
{
    SpriteRenderer uiTarned;
    SpriteRenderer bellySR;
    [SerializeField] Sprite spriteWhole;
    [SerializeField] Sprite spriteRipped;
    [SerializeField] LayerMask ponyLayer;
    Transform uiEnemy;

    [SerializeField] AudioSource bustedAS;
    [SerializeField] AudioSource winAS;

    bool groupHidden = false;
    bool foodHidden = false;
    bool inBarbiesView = false;
    bool played = false;
    Transform ripped = null;
    int enemyShow = 0;

    float cameraStartTime = 0;

    Barbie barbie;

    Transform bodyHead;
    Transform bodyButt;

    // Start is called before the first frame update
    void Start()
    {
        uiEnemy = transform.Find("UI-Enemy");
        uiTarned = transform.Find("UI-tarned").GetComponent<SpriteRenderer>();
        bellySR = transform.Find("Sprite").GetComponent<SpriteRenderer>();
        uiTarned.enabled = false;
        uiEnemy.gameObject.SetActive(false);
        bellySR.sprite = spriteWhole;

        bodyHead = GameObject.Find("HorseHead").transform;
        bodyButt = GameObject.Find("HorseButt").transform;

        GLOBAL.horseHidden = false;
        GLOBAL.enemy = null;
        GLOBAL.attention = 0;

        barbie = GameObject.Find("Barbie").GetComponent<Barbie>(); 
    }

    // Update is called once per frame
    void Update()
    {
        #region camera controll
        if (GLOBAL.enemy == null) Camera.main.transform.position = transform.position + Vector3.back * 10 + Vector3.up * 1.5f;
        else
        {
            if (!played)
            {
                bustedAS.Play();
                played = true;
            }

            if (cameraStartTime == 0) cameraStartTime = Time.time;

            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, GLOBAL.enemy.position + Vector3.back * 10 + Vector3.up * 1.5f, (Time.time - cameraStartTime) / 3);
        }
        #endregion
    }

    private void LateUpdate()
    {
        if (enemyShow == 0 && uiEnemy.gameObject.activeSelf) uiEnemy.gameObject.SetActive(false);
        else enemyShow--;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "EnemyView" && !GLOBAL.horseHidden)
        {
            other.transform.parent.GetComponent<Dealer>().SeeingTheHorse();

            if (GLOBAL.attention > 0)
            {
                if (!uiEnemy.gameObject.activeSelf) uiEnemy.gameObject.SetActive(true);

                uiEnemy.LookAt(other.transform.position);
                enemyShow = 2;
            }
        }
        else if (other.tag == "BarbieView" && !GLOBAL.horseHidden && GLOBAL.barbieVisible)
        {
            barbie.SetPlayerVisible(true);
            inBarbiesView = true;
        }

        if (other.tag == "Finish" && GLOBAL.collectedObjects > 3)
        {
            GameObject.Find("Canvas").GetComponent<CanvasCtrl>().ShowWinscreen();

            if (!played)
            {
                winAS.Play();
                Camera.main.GetComponent<AudioSource>().Stop();
                played = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "BarbieView")
        {
            barbie.SetPlayerVisible(false);
            inBarbiesView = false;
        }
    }

    void FixedUpdate()
    {
        Collider2D[] cols = new Collider2D[6];

        Physics2D.OverlapCircleNonAlloc(transform.position, 0.5f, cols, ponyLayer);

        int ponyCounter = 0;
        foreach (Collider2D col in cols)
        {
            if (col != null) ponyCounter++;
        }

        if (ponyCounter > 1) HiddenInGroup(true);
        else HiddenInGroup(false);

        if (inBarbiesView)
        {
            if (GLOBAL.horseHidden || !GLOBAL.barbieVisible)
            {
                barbie.SetPlayerVisible(false);
                inBarbiesView = false;
            }
        }

        inBarbiesView = false;

        //flip to stitched body
        if (ripped != null)
        {
            if (ripped.position.x < transform.position.x) bellySR.transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
            else bellySR.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        }
        else
        {
            Vector2 distances = new Vector2(Mathf.Abs(bodyHead.position.x - bodyButt.position.x), Mathf.Abs(bodyHead.position.y - bodyButt.position.y));
            distances = new Vector2(Mathf.Clamp(distances.x, 1, 2), Mathf.Clamp(distances.y, 1, 2));

            //if (distances.x > distances.y) bellySR.transform.localScale = new Vector3(-0.2f * distances.x, 0.2f, 0.2f);
            //else bellySR.transform.localScale = new Vector3(-0.2f, 0.2f * distances.y, 0.2f);

            bellySR.transform.localScale = new Vector3(-0.2f * distances.x, 0.2f * distances.y, 0.2f);
        }
    }

    void HiddenInGroup(bool hidden)
    {
        groupHidden = hidden;

        Hidden();
    }

    public void HiddenEating(bool hidden)
    {
        foodHidden = hidden;

        Hidden();
    }

    public void Ripped(Transform trans)
    {
        ripped = trans;
        bellySR.sprite = spriteRipped;
    }

    void Hidden()
    {
        if (foodHidden || groupHidden) GLOBAL.horseHidden = true;
        else GLOBAL.horseHidden = false;

        uiTarned.enabled = GLOBAL.horseHidden;
    }
}
