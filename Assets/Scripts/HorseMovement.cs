﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HorseMovement : MonoBehaviour {

	[SerializeField] HorseMovement partner;
    [SerializeField] Animator animatorLegs;
    [SerializeField] float forceMultiplier;
    [SerializeField] KeyCode keyInteract;
    [SerializeField] LayerMask interactionMask;
    [SerializeField] float animationMultiplier;
    [SerializeField] float maxSpeed;
    public float direction;
    public bool head;

    [SerializeField] AudioSource stepsAS;
    [SerializeField] AudioSource eatingAS;
    [SerializeField] AudioSource tearingAS;
    [SerializeField] AudioClip[] stepsClips;
    [SerializeField] AudioClip[] eatingClips;
    [SerializeField] AudioClip[] takeClips;

    public Vector3 inputVector;
    HorseControl horseControl;

    bool dead = false;
    bool eating = false;
    bool grabbing = false;

    Rigidbody2D rb;
    Transform interacter;
    Transform sprite;
    SpriteRenderer[] sr;
    int[] addToOrder;

    Transform eatingParticles;

    // Start is called before the first frame update
    void Start() {
        interacter = transform.Find("Interacter");
        sprite = transform.Find("Sprite");

        rb = GetComponent<Rigidbody2D>();
        sr = GetComponentsInChildren<SpriteRenderer>();

        horseControl = GameObject.Find("HorseBelly").GetComponent<HorseControl>();
        if (head)
        {
            eatingParticles = GameObject.Find("EatingHorse").transform;
            eatingParticles.gameObject.SetActive(false);
        }

        animatorLegs.SetBool("Butt", !head);

        addToOrder = new int[sr.Length];

        for (int i = 0; i < sr.Length; i++) addToOrder[i] = sr[i].sortingOrder;
    }

    // Update is called once per framevoid
    void Update() {
        #region movement
        inputVector = Vector3.zero;
        float speed = 0;

        if (!dead)
        {
            if (head && !eating)
            {
                inputVector = new Vector3(Input.GetAxis("Horizontal_P1"), Input.GetAxis("Vertical_P1"), 0);
            }
            else if (!head && !grabbing)
            {
                inputVector = new Vector3(Input.GetAxis("Horizontal_P2"), Input.GetAxis("Vertical_P2"), 0);
            }
        }

        rb.AddForce(inputVector * Time.deltaTime * forceMultiplier);
        speed = rb.velocity.magnitude;

        if (inputVector == Vector3.zero) animatorLegs.SetFloat("Force", Mathf.Clamp01(speed/maxSpeed) * -1f);
        else animatorLegs.SetFloat("Force", Mathf.Clamp01(speed / maxSpeed));
        #endregion

        #region step sounds
        if (speed > 0.1f)
        {
            int treshold = 6;
            if (speed / maxSpeed > 0.3f) treshold = 4;
            else if (speed / maxSpeed > 0.7f) treshold = 2;

            if ((int) (Time.time*10 % treshold) == 0)
            {
                stepsAS.clip = stepsClips[(int)Random.Range(0, stepsClips.Length)];
                if (!stepsAS.isPlaying) stepsAS.Play();
            }
        }
        #endregion

        #region interaction
        if (Input.GetKey(keyInteract))
        {
            if (head)
            {
                if (Physics2D.OverlapCircle(interacter.position, 0.5f, interactionMask))
                {
                    horseControl.HiddenEating(true);
                    animatorLegs.SetBool("Eating", true);
                    eatingParticles.gameObject.SetActive(true);
                    eatingParticles.position = GameObject.Find("HeadParticles").transform.position;
                    eating = true;

                    //sound
                    if (!eatingAS.isPlaying)
                    {
                        eatingAS.clip = eatingClips[(int)Random.Range(0, eatingClips.Length)];
                        eatingAS.Play();
                    }
                }
                else
                {
                    horseControl.HiddenEating(false);
                    animatorLegs.SetBool("Eating", false);
                    eatingParticles.gameObject.SetActive(false);
                    eating = false;
                }
            }
        }

        if (Input.GetKeyUp(keyInteract)) {
            if (head)
            {
                horseControl.HiddenEating(false);
                animatorLegs.SetBool("Eating", false);
                eatingParticles.gameObject.SetActive(false);
                eating = false;
            }
        }

        if (Input.GetKeyDown(keyInteract))
        {
            if (!head)
            {
                Collider2D[] cols = new Collider2D[1];

                Physics2D.OverlapCircleNonAlloc(interacter.position, 0.5f, cols, interactionMask);

                animatorLegs.SetTrigger("Grab");

                if (cols[0] != null)
                {
                    grabbing = true;
                    StartCoroutine(GrabToHand(cols[0].transform));
                    GameObject.Find("Canvas").GetComponent<CanvasCtrl>().ObjectCollected(cols[0].name);
                }
            }
        }

        if (head && eating && !Input.GetKey(keyInteract))
        {
            horseControl.HiddenEating(false);
            animatorLegs.SetBool("Eating", false);
            eating = false;
        }
        #endregion

        //get body direction
        if (transform.position.x < horseControl.transform.position.x) direction = -1;
        else direction = 1;

        interacter.localPosition = Vector3.right * 0.8f * direction;
        if (head) sprite.localScale = new Vector3(direction, 1, 0);
        else sprite.localScale = new Vector3(direction*-1, 1, 0);

        //sprite order
        for (int i = 0; i < sr.Length; i++)
        {
            sr[i].sortingOrder = Mathf.RoundToInt(transform.position.y * -50) + addToOrder[i];
        }
    }

    void OnJointBreak2D(Joint2D brokenJoint) {
        BrokenRelationship();
        partner.BrokenRelationship();
        StartCoroutine("RestartGame");

        tearingAS.Play();

        if (head) GameObject.Find("HorseBelly").GetComponent<HorseControl>().Ripped(GameObject.Find("HorseButt").transform);
        else GameObject.Find("HorseBelly").GetComponent<HorseControl>().Ripped(GameObject.Find("HorseHead").transform);

        Debug.Log("The broken joint exerted a reaction force of " + brokenJoint.reactionForce);
    }

    public void BrokenRelationship()
    {
        dead = true;
        rb.drag = 1;
    }

    IEnumerator RestartGame()
    {
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator GrabToHand(Transform trans)
    {
        yield return new WaitForSeconds(1f);

        trans.GetComponent<GrabHand>().GrabToHand();

        eatingAS.clip = takeClips[(int)Random.Range(0, takeClips.Length)];
        eatingAS.Play();

        yield return new WaitForSeconds(1f);

        trans.gameObject.SetActive(false);
        grabbing = false;
    }
}
