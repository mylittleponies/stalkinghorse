﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRandomizer : MonoBehaviour
{
	private Animator animator;

    void Start()
    {
		animator = gameObject.GetComponentInChildren<Animator> ();
		animator.SetFloat ("Random", Random.value);
    }
}
