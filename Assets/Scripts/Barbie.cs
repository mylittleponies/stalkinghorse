﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barbie : MonoBehaviour
{
    [SerializeField] float rightLimit;
    [SerializeField] float leftLimit;
    [SerializeField] float direction;
    [SerializeField] float speed;

    [SerializeField] AudioClip[] lalalaClips;
    [SerializeField] AudioClip[] seeingClips;
    [SerializeField] AudioClip[] nothingClips;

    public Animator animator;
    public bool seeingPlayer = false;

    AudioSource auSo;
    Rigidbody2D rb;
    CanvasCtrl canvasCtrl;

    Transform barbieTrigger;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetBool("Walk", true);

        rb = GetComponent<Rigidbody2D>();
        auSo = GetComponent<AudioSource>();

        canvasCtrl = GameObject.Find("Canvas").GetComponent<CanvasCtrl>();

        barbieTrigger = GameObject.Find("BarbieTrigger").transform;

        StartCoroutine(PlayLalala(0));
    }

    // Update is called once per frame
    void Update()
    {
        #region movement
        if (animator.GetBool("Walk"))
        {
            if (direction > 0)
            {
                if (transform.position.x < rightLimit)
                {
                    rb.MovePosition(transform.position + Vector3.right * speed);
                }
                else
                {
                    direction *= -1;
                    transform.GetChild(0).localScale = new Vector3(0.25f, 0.25f, 0);
                }
            }
            else if (direction < 0)
            {
                if (transform.position.x > leftLimit)
                {
                    rb.MovePosition(transform.position + Vector3.left * speed);
                }
                else
                {
                    direction *= -1;
                    transform.GetChild(0).localScale = new Vector3(-0.25f, 0.25f, 0);
                }
            }
        }
        #endregion

        #region check if visible
        if (GLOBAL.barbieVisible)
        {
            Vector2 barbPos = Camera.main.WorldToViewportPoint(transform.position);

            if (barbPos.x < 0 || barbPos.x > 1 || barbPos.y < 0 || barbPos.y > 1)
            {
                Debug.Log("Barbie is invisible");
                GLOBAL.barbieVisible = false;
                seeingPlayer = false;
            }
        }
        else
        {
            Vector2 barbPos = Camera.main.WorldToViewportPoint(transform.position);

            if (barbPos.x > 0 && barbPos.x < 1 && barbPos.y > 0 && barbPos.y < 1)
            {
                Debug.Log("Barbie is visible");
                GLOBAL.barbieVisible = true;
            }
        }
        #endregion

        if (seeingPlayer)
        {
            canvasCtrl.AddAttention(transform, 3f);
        }
    }

    public void SetPlayerVisible(bool visible)
    {
        if (visible != seeingPlayer)
        {
            seeingPlayer = visible;

            Debug.Log("Barbie sees: " + visible);

            if (seeingPlayer) barbieTrigger.localScale = new Vector3(1, 2, 1);
            else barbieTrigger.localScale = new Vector3(1, 1, 1);

            #region sound
            bool stepSound = true;
            if (auSo.isPlaying)
            {
                for (int i = 0; i < seeingClips.Length; i++)
                {
                    if (auSo.clip == seeingClips[i]) stepSound = false;
                }
                for (int i = 0; i < nothingClips.Length; i++)
                {
                    if (auSo.clip == nothingClips[i]) stepSound = false;
                }
            }

            if (stepSound)
            {
                if (visible)
                {
                    auSo.Stop();
                    auSo.clip = seeingClips[(int)Random.Range(0, seeingClips.Length)];
                    auSo.Play();
                }
                else
                {
                    auSo.Stop();
                    auSo.clip = nothingClips[(int)Random.Range(0, nothingClips.Length)];
                    auSo.Play();
                }
            }
            #endregion
        }

        StartCoroutine(SetWalk(!visible));
    }

    IEnumerator SetWalk(bool walk)
    {
        yield return new WaitForSeconds(0.3f);

        animator.SetBool("Walk", walk);
    }

    IEnumerator PlayLalala(int lastClip)
    {
        yield return new WaitForSeconds(Random.Range(1.5f, 3f));

        //sound
        if (animator.GetBool("Walk") && !auSo.isPlaying)
        {
            lastClip += (int)Random.Range(1, lalalaClips.Length-1);
            if (lastClip >= lalalaClips.Length) lastClip -= lalalaClips.Length;

            auSo.clip = lalalaClips[lastClip];
            auSo.Play();
        }

        StartCoroutine(PlayLalala(lastClip));
    }
}
