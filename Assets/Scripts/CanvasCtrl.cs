﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasCtrl : MonoBehaviour
{
    [SerializeField] Color colorNormal;
    [SerializeField] Color colorCollected;
    [SerializeField] float attentionMax;
    [SerializeField] AnimationCurve showWindow;

    Image object1;
    Image object2;
    Image object3;
    Slider attentionSlider;

    RectTransform winScreen;
    AudioSource dealerAS;

    [SerializeField] AudioClip[] seeingClips;
    [SerializeField] AudioClip[] nothingClips;

    float showWindowTimer = 0;

    bool wasDealer = false;

    int seeingHorse = 0;

    // Start is called before the first frame update
    void Start()
    {
        object1 = transform.Find("Object1").GetComponent<Image>();
        object2 = transform.Find("Object2").GetComponent<Image>();
        object3 = transform.Find("Object3").GetComponent<Image>();
        attentionSlider = transform.Find("Visibility").GetComponent<Slider>();

        winScreen = transform.Find("Winscreen").GetComponent<RectTransform>();
        winScreen.gameObject.SetActive(false);

        dealerAS = GetComponent<AudioSource>();

        object1.color = colorNormal;
        object2.color = colorNormal;
        object3.color = colorNormal;

        GLOBAL.collectedObjects = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (seeingHorse == 0 && GLOBAL.attention > 0 && GLOBAL.attention < attentionMax)
        {
            Debug.Log("Deacrease attention: " + seeingHorse);
            GLOBAL.attention -= Time.fixedDeltaTime * 2;

            if (GLOBAL.attention <= 0) GLOBAL.attention = 0;

            UpdateUIAttention();
        }

        if (seeingHorse > 0)
        {
            seeingHorse--;

            if (seeingHorse == 0 && !dealerAS.isPlaying && wasDealer)
            {
                dealerAS.clip = nothingClips[(int)Random.Range(0, nothingClips.Length)];
                dealerAS.Play();
            }
        }

        #region winscreen
        if (winScreen.gameObject.activeSelf)
        {
            if (showWindowTimer < 2)
            {
                float factor;
                showWindowTimer += Time.fixedDeltaTime;

                factor = showWindowTimer / 2;

                winScreen.anchoredPosition3D = Vector3.Lerp(Vector3.up * 500, Vector3.down * 600, showWindow.Evaluate(factor));
            }
        }
        #endregion
    }

    public void ObjectCollected(string objectName)
    {
        GLOBAL.collectedObjects++;

        if (GLOBAL.collectedObjects > 3)
        {
            GameObject.Find("Tutorial").SetActive(false);
            transform.Find("Text").GetComponent<Text>().text = "Now return to the first room and leave!";
        }

        transform.Find(objectName).GetComponent<Image>().color = colorCollected;
    }

    public void AddAttention(Transform trans, float multiplicator)
    {
        if (seeingHorse == 0 && trans.name.Contains("Dealer") && !dealerAS.isPlaying)
        {
            dealerAS.clip = seeingClips[(int)Random.Range(0, seeingClips.Length)];
            dealerAS.Play();
            wasDealer = true;
        }

        if (!trans.name.Contains("Dealer")) wasDealer = false;

        GLOBAL.attention += Time.deltaTime * multiplicator;
        seeingHorse = 2;

        if (GLOBAL.attention >= attentionMax)
        {
            GLOBAL.enemy = trans;
            StartCoroutine("RestartGame");
        }

        UpdateUIAttention();
    }

    public void ShowWinscreen()
    {
        if (!winScreen.gameObject.activeSelf)
        {
            winScreen.gameObject.SetActive(true);
            showWindowTimer = Time.deltaTime;
            StartCoroutine("ReturnToMenu");
        }
    }

    void UpdateUIAttention()
    {
        attentionSlider.value = GLOBAL.attention / attentionMax;
    }

    IEnumerator RestartGame()
    {
        yield return new WaitForSeconds(3.5f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator ReturnToMenu()
    {
        yield return new WaitForSeconds(12f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
    }
}
