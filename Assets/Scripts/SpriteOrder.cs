﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpriteOrder : MonoBehaviour
{
    public bool onlyAtPlay;
    public bool particleSystem;
    SpriteRenderer[] sr;
    int[] addToOrder;

    void Awake()
    {
        if (particleSystem) GetComponent<ParticleSystemRenderer>().sortingOrder = Mathf.RoundToInt(transform.position.y * -50) + 50;
        else
        {
            sr = GetComponentsInChildren<SpriteRenderer>();
            addToOrder = new int[sr.Length];

            for (int i = 0; i < sr.Length; i++)
            {
                if (onlyAtPlay) addToOrder[i] = sr[i].sortingOrder;
                else addToOrder[i] = 0;
            }
        }
    }

    private void Start()
    {

    }

    // Update is called once per frame
    public void Update()
    {
        if (particleSystem) GetComponent<ParticleSystemRenderer>().sortingOrder = Mathf.RoundToInt(transform.position.y * -50) + 50;
        else {
            if ((Application.isEditor && !onlyAtPlay) || Application.isPlaying)
            {
                for (int i = 0; i < sr.Length; i++)
                {
                    sr[i].sortingOrder = Mathf.RoundToInt(transform.position.y * -50) + addToOrder[i];
                }
            }
        }

    }
}
