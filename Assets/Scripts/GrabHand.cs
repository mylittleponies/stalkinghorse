﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabHand : MonoBehaviour
{
    bool grabbed = false;
    Transform hand;

    // Start is called before the first frame update
    void Start()
    {
        hand = GameObject.Find("Hand").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (grabbed) transform.position = hand.position;
    }

    public void GrabToHand()
    {
        grabbed = true;
    }
}
